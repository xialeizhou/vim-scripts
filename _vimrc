"Note when ~/.vimrc present the following is done automatically
"set nocompatible
"
""""""""""""""""""""""""""""""""""""""""""""""""
" key mappings
""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = "\\"
nmap <leader>sg : %s/googleapis/useso/g <CR>
nmap <leader>t8 :set tenc=utf8 <CR>
nmap <leader>tg :set tenc=gbk <CR>
nmap <leader>e8 :set enc=utf-8 <CR> :set fenc=utf-8 <CR>
nmap <leader>eg :set enc=gbk <CR> :set fenc=gbk <CR>
nmap <leader>f8 :set fenc=utf-8 <CR>
nmap <leader>fg :set fenc=gbk <CR>
"get current time
nmap <F3> "=strftime("%c")<CR>P
nmap <leader>p :Project <CR> "open project plugin
"靠trails
nmap <leader>x :%s/\s\+$//e <CR>

""""""""""""""""""""""""""""""""""""""""""""""""
" plugin settings
""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = ","
"Cscope settings
if has("cscope")
  set csprg=/usr/local/bin/cscope
  set csto=0
  set cst
  set cscopequickfix=s-,c-,d-,i-,t-,e-
  set nocsverb
  "if filereadable("cscope.out")
  "    cs add cscope.out
  "elseif $CSCOPE_DB != ""
  "    cs add $CSCOPE_DB
  "endif
  set csverb
endif
"map <leader><leader>n :cs add ./cscope.out .<CR><CR><CR> :cs reset<CR>
"imap <leader> <leader>i:cs add ./cscope.out .<CR><CR><CR> :cs reset<CR>
"s: Find this C symbol
nmap <leader>s :cs find s <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>
"g: Find this definition
nmap <leader>g :cs find g <C-R>=expand("<cword>")<CR><CR>
"d: Find functions called by this function
nmap <leader>d :cs find d <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>
"c: Find functions calling this function
nmap <leader>c :cs find c <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>
"t: Find assignments to
nmap <leader>t :cs find t <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>
"e: Find this egrep pattern
nmap <leader>e :cs find e <C-R>=expand("<cword>")<CR><CR> :copen<CR><CR>
"f: Find this file
nmap <leader>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
"i: Find files #including this file
nmap <leader>i :cs find i <C-R>=expand("<cfile>")<CR><CR> :copen<CR><CR>

"================================================================
"                       Vundle Manager
"================================================================
"Vundle settings
set nocompatible               " be iMproved
filetype off                   " required!
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
"
"My vundle repost settings
"Keep Plugin commands between vundle#begin/end.
"---------
"PowerLine
Plugin 'Lokaltog/vim-powerline'
set laststatus=2
let g:Powline_symbols='fancy'

"---------
"colors
" Solarized/molokai theme
Plugin 'altercation/vim-colors-solarized'
Plugin 'tomasr/molokai'
let g:solarized_termcolors=256
let g:solarized_termtrans=1
let g:solarized_contrast="normal"
let g:solarized_visibility="normal"

" theme molokai
let g:molokai_original = 1
" color solotion
set t_Co=256

syntax enable
"set background=light
"set background=light
colorscheme molokai
"colorscheme solarized
"colorscheme phd
"---------
Plugin 'ZenCoding.vim'

"---------
"tag bar navigation, Dimension is different with taglist
Plugin 'majutsushi/tagbar'
nmap <leader>tb :TagbarToggle<CR>
let g:tagbar_autofocus = 0
"let g:tagbar_left = 1
let g:tagbar_right = 0
let g:tagbar_width = 40
let g:tagbar_autoclose = 1
let g:tagbar_sort = 1
let g:tagbar_indent = 1
let g:tagbar_show_visibility = 1
let g:tagbar_show_linenumbers = 2
"autocmd FileType * nested :call tagbar#autoopen(0)
"autocmd BufEnter * nested :call tagbar#autoopen(0)
highlight TagbarScope guifg=Green ctermfg=Green
"---------
"taglist
Plugin 'vim-scripts/taglist.vim'
"taglist settings
nmap <silent> <leader>tl :Tlist <CR>
let Tlist_Set_Menu=1
let Tlist_Ctags_Cmd='ctags'
let Tlist_Use_Right_Window=0
let Tlist_Show_One_File=0
let Tlist_File_Fold_Auto_Close=0
let Tlist_Exit_OnlyWindow=1
let Tlist_Process_File_Always=1
let Tlist_Inc_Winwidth=0
"---------
"brackets highlightt
Plugin 'kien/rainbow_parentheses.vim'
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]
let g:rbpt_max = 40
let g:rbpt_loadcmd_toggle = 0
"---------
"Visualization of the indentation
Plugin 'Indent-Guides'
let g:indent_guides_enable_on_vim_startup = 1  " default closed
let g:indent_guides_guide_size            = 1  " indeent size
let g:indent_guides_start_level           = 2  " start indent from senecond line
"Use this option to specify a list of filetypes to disable the plugin for.
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree'] 
"When `set background=dark` is used, the following highlight colors will be
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey
"Use this option to control whether or not the plugin automatically calculates 
"the highlight colors. 
"the highlight colors. Will use the current colorscheme's background color as a
let g:indent_guides_auto_colors = 1
"\ig 打开/关闭 vim-indent-guides
nmap <Leader>i <Plug>IndentGuidesToggle
"---------
"winmanager
Plugin 'The-NERD-tree'
let g:NERDTree_title = "[NERDTree]"
let NERDTreeShowBookmarks=1
let NERDTreeHighlightCursorline=1
let NERDTreeShowFiles=1
let NERDTreeShowHidden=1
let NERDTreeWinPos=0
let NERDTreeShowLineNumbers=1
let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.obj$', '\.o$', '\.so$', '\.egg$', '^\.git$', '^\.svn$', '^\.hg$', '^\.swf' ,'^\.db$','^\.ico$','^\.txt$','^\.sql$', '\.in$','\.po$','\.out$']
let g:netrw_home='~/bak'

"autocmd vimenter * NERDTree
"close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | end
function! NERDTree_Start()
    exec 'NERDTree'
endfunction

function! NERDTree_IsValid()
    return 1
endfunction
"---------
"add comment quickly
Plugin 'The-NERD-Commenter'
"<leader>cc: Comment out the current line or text selected in visual mode.
"<leader>ci: Toggles the comment state of the selected line(s) individually.
"---------
""winmanager settings
Plugin 'winmanager'
"let g:winmanagerwindowlayout = "taglist"
let g:winManagerWindowLayout='NERDTree|BufExplorer'
"let g:winmanagerwindowlayout='NERDTree|taglist' " fileexplorer) or (fileexplorer|taglist)
let g:persistentbehaviour=0
let g:winManagerWidth = 30
let g:AutoOpenWinManager = 1
nmap <silent><leader>wm :if IsWinManagerVisible() <BAR> WMToggle<CR> <BAR> else <BAR> WMToggle<CR>:q<CR> endif <CR><CR>
"nmap <leader>wm :WMToggle<cr>
nmap <leader>nt     :NERDTree <CR> :set rnu<CR>
nmap <leader>ntc    :NERDTreeClose <CR>
"let NERDChristmasTree=0

"nmap <silent><leader>wm :if IsWinManagerVisible() <BAR> WMToggle<CR> <BAR> else <BAR> WMToggle<CR>:q<CR> endif <CR><CR>
"---------
Plugin 'winwkspaceexplorer'
"---------
""for code alignment
Plugin 'Tabular'
nmap <leader>bb :Tab /=<CR>
nmap <leader>bn :Tab /]
"---------
"easy move
"// + w/f/l
Plugin 'EasyMotion'
let g:EasyMotion_leader_key = '<Leader>'
" Mapping           | Details
" ------------------|----------------------------------------------
" <Leader>f{char}   | Find {char} to the right. See |f|.
" <Leader>F{char}   | Find {char} to the left. See |F|.
" <Leader>t{char}   | Till before the {char} to the right. See |t|.
" <Leader>T{char}   | Till after the {char} to the left. See |T|.
" <Leader>w         | Beginning of word forward. See |w|.
" <Leader>W         | Beginning of WORD forward. See |W|.
" <Leader>b         | Beginning of word backward. See |b|.
" <Leader>B         | Beginning of WORD backward. See |B|.
" <Leader>e         | End of word forward. See |e|.
" <Leader>E         | End of WORD forward. See |E|.
" <Leader>ge        | End of word backward. See |ge|.
" <Leader>gE        | End of WORD backward. See |gE|.
" <Leader>j         | Line downward. See |j|.
" <Leader>k         | Line upward. See |k|.
" <Leader>n         | Jump to latest "/" or "?" forward. See |n|.
" <Leader>N         | Jump to latest "/" or "?" backward. See |N|.
"
"---------
"macro auto completep
" Track the engine.
"---------
"auto pair bracket
Plugin 'delimitMate.vim'
au FileType mail let b:delimitMate_autoclose = 0
"---------
"Autodetect multiple encodings
Plugin 'FencView.vim'
let g:fencview_autodetect=1
"---------
" syntax analysis
Plugin 'Syntastic'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_error_symbol = '✗'
let g:syntastic_warning_symbol = '⚠'

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_loc_list_height = 6
let g:syntastic_enable_highlighting = 1

let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"let g:syntastic_error_symbol = 'x'
"let g:syntastic_warning_symbol = '!' "set error or warning signs
"let g:syntastic_check_on_open=1
"let g:syntastic_enable_highlighting = 1
"let g:syntastic_cpp_compiler = "g++"
"let g:syntastic_python_checker="flake8,pyflakes,pep8,pylint"
"let g:syntastic_python_checkers=['pyflakes']
"let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd']
"let g:syntastic_c_checkers=['make','splint']
"let g:syntastic_ruby_checkers = ['ruby']
"let g:syntastic_cpp_checkers=['gcc','cppcheck', 'cpplint']
"let g:syntastic_always_populate_loc_list = 2
"let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'
"highlight SyntasticErrorSign guifg=white guibg=black
"let g:syntastic_mode_map = { "mode": "active",
                           "\ "active_filetypes": ["ruby", "php"],
                           "\ "passive_filetypes": ["puppet"] }
"let g:syntastic_cpp_include_dirs = ['/usr/include/']
"let g:syntastic_cpp_remove_include_errors = 1
"let g:syntastic_cpp_check_header = 1
"let g:syntastic_enable_balloons = 1     "whether to show balloons
"---------
"MiniBufferExplorer settings begin
"Plugin 'minibufexplorerpp' 
"let g:miniBufExplMapWindowNavVim = 1
"let g:miniBufExplMapWindowNavArrows = 1
"let g:miniBufExplMapCTabSwitchBufs = 1
"let g:miniBufExplMapCTabSwitchWindows = 1 
"let g:miniBufExplModSelTarget = 1

"---------
"Plugin 'Doxygen-via-Doxygen'
Plugin 'DoxygenToolkit.vim'
"DoxygenToolkit settings
let g:DoxygenToolkit_authorName="xialeizhou@gmail.com"
let g:DoxygenToolkit_briefTag_pre="@文投在线"
let s:licenseTag = "Copyright (C) 2014 Wentou Inc. \<enter>"
let s:licenseTag = s:licenseTag . "All rights reserved."
let g:DoxygenToolkit_licenseTag=s:licenseTag
let g:DoxygenToolkit_undocTag="DOXIGEN_SKIP_BLOCK"
let g:DoxygenToolkit_briefTag_pre = "@brief\t"
let g:DoxygenToolkit_classTag = "@class\t"
let g:DoxygenToolkit_paramTag_pre = "@param\t"
let g:DoxygenToolkit_returnTag = "@return\t"
let g:DoxygenToolkit_briefTag_funcName = "yes"
let g:DoxygenToolkit_versionString="0.1.0"
let g:DoxygenToolkit_maxFunctionProtoLines = 30

nmap <leader>da :DoxAuthor <CR>
nmap <leader>dx :Dox <CR>
nmap <leader>dl :DoxLic <CR>
"---------
"for file search ctrlp, search files
Plugin 'ctrlp.vim'
" open ctrlp to search
let g:ctrlp_map = '<leader>grep'
let g:ctrlp_cmd = 'CtrlP'
"show recently opened files
map <leader>fp :CtrlPMRU<CR>
"set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux"
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn|rvm)$',
    \ 'file': '\v\.(exe|so|dll|zip|tar|tar.gz)$',
    \ }
"\ 'link': 'SOME_BAD_SYMBOLIC_LINKS',
let g:ctrlp_working_path_mode=0
let g:ctrlp_match_window_bottom=1
let g:ctrlp_max_height=15
let g:ctrlp_match_window_reversed=0
let g:ctrlp_mruf_max=500
let g:ctrlp_follow_symlinks=1
"---------
Plugin 'rails.vim'
"===Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'

"===4.git repos on your local machine
"Plugin 'file:///home/gmarik/path/to/plugin'
"---------
call vundle#end()            " required
filetype plugin indent on     " required!
" vbundle's key maps
"list configured bundles
nmap <leader>pl  :PluginList     <CR>
"install bundles
nmap <leader>pi  :PluginInstall!<CR>
"search(or refresh cache first) for foo
nmap <leader>ps  :PluginSearch! <CR>
"auto-approve removal of unused bundles
nmap <leader>pc  ::PluginClean! <CR>

"================================================================
"                       Vundle Manager End
"================================================================

"================================================================
" code settings
"================================================================
"code folder
set foldmethod=manual
set foldlevel=100
set foldcolumn=5

"encoding settings
if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
    set fencs=utf-8,gbk,latin1
endif
set tenc=utf-8
set enc=utf-8
set nu

"tab control mappings
nmap <C-N> :tabnext<CR>
nmap <C-P> :tabprev<CR>

"allow backspacing over everything in insert mode
set bs=2
set viminfo='20,\"50    " read/write a .viminfo file, don't store more
                        " than 50 lines of registers
set history=50          " keep 50 lines of command line history
"Allow switching buffers without writing to disk
set hidden
"Always show cursor position
set ruler
"Set terminal title to filename
set title
if has("autocmd")
  " When editing a file, always jump to the last cursor position
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal g'\"" |
  \ endif
endif
"This is necessary to allow pasting from outside vim. It turns off auto stuff.
"You can tell you are in paste mode when the ruler is not visible
set pastetoggle=<F2>
"Usually annoys me
set nowrap
"Usually I don't care about case when searching
set ignorecase
"Only ignore case when we type lower case when searching
set smartcase
"I hate noise. The t_vb bit removes any delay also
set visualbell t_vb=
"Show menu with possible tab completions
set wildmenu
"Ignore these files when completing names and in Explorer
set wildignore=.svn,CVS,.git,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif

"Search for tags in this dir and above
set tag=/Users/xialeizhou/workshop/autonavi/DataCompiler/tags
"set tags=./TAGS;/,./tags;/
"et tags="/home/ns-lsp/webroot/wentou/tags"
"set tags+='/home/ns-lsp/webroot/wentou/extensions/tags.uc_server'
" Alt-right/left to navigate forward/backward in the tags stack
"map <M-Left> <C-T>
"map <M-Right> <C-]>

""""""""""""""""""""""""""""""""""""""""""""""""
" Indenting
""""""""""""""""""""""""""""""""""""""""""""""""

"Default to autoindenting of C like languages
"This is overridden per filetype below
set noautoindent smartindent

"The rest deal with whitespace handling and
"mainly make sure hardtabs are never entered
"as their interpretation is too non standard in my experience
set softtabstop=2
" Note if you don't set expandtab, vi will automatically merge
" runs of more than tabstop spaces into hardtabs. Clever but
" not what I usually want.
set expandtab
set shiftwidth=2
set shiftround
set nojoinspaces

""""""""""""""""""""""""""""""""""""""""""""""""
" Dark background
""""""""""""""""""""""""""""""""""""""""""""""""

"I always work on dark terminals
set background=dark

"Make the completion menus readable
highlight Pmenu ctermfg=0 ctermbg=3
highlight PmenuSel ctermfg=0 ctermbg=7

"The following should be done automatically for the default colour scheme
"at least, but it is not in Vim 7.0.17.
if &bg == "dark"
  highlight MatchParen ctermbg=darkblue guibg=blue
endif

""""""""""""""""""""""""""""""""""""""""""""""""
" Syntax highlighting
""""""""""""""""""""""""""""""""""""""""""""""""

"The following are a bit slow
"for me to enable by default
set cursorline   "highlight current line
set cursorcolumn "highlight current column

"Display a visual barrier for col 80
"This has the disadvantage of adding trailing whitespace
"when copy and pasting from vim.
"set colorcolumn=80

if &t_Co >= 256 && &bg == "dark"
  highlight ColorColumn ctermbg=233
elseif &t_Co >= 256 && &bg != "dark"
  highlight ColorColumn ctermbg=230
else
  highlight ColorColumn ctermbg=8
endif

if &diff
    "I'm only interested in diff colours
    syntax off
endif

"syntax highlight shell scripts as per POSIX,
"not the original Bourne shell which very few use
let g:is_posix = 1

"flag problematic whitespace (trailing and spaces before tabs)
"Note you get the same by doing let c_space_errors=1 but
"this rule really applys to everything.
highlight RedundantSpaces term=standout ctermbg=red guibg=red
match RedundantSpaces /\s\+$\| \+\ze\t/ "\ze sets end of match so only spaces highlighted
"use :set list! to toggle visible whitespace on/off
set listchars=tab:>-,trail:.,extends:>

""""""""""""""""""""""""""""""""""""""""""""""""
" Key bindings
""""""""""""""""""""""""""""""""""""""""""""""""

"Note <leader> is the user modifier key (like g is the vim modifier key)
"One can change it from the default of \ using: let mapleader = ","
"
let mapleader = "\\"
"\n to turn off search highlighting
nmap <silent> <leader>n :silent :nohlsearch<CR>
"\l to toggle visible whitespace
nmap <silent> <leader>l :set list!<CR>
"Shift-tab to insert a hard tab
imap <silent> <S-tab> <C-v><tab>

"allow deleting selection without updating the clipboard (yank buffer)
vnoremap x "_x
vnoremap X "_X

"don't move the cursor after pasting
"(by jumping to back start of previously changed text)
noremap p p`[
noremap P P`[

"Reselect after indent so it can easily be repeated
vnoremap < <gv
vnoremap > >gv

"<home> toggles between start of line and start of text
imap <khome> <home>
nmap <khome> <home>
inoremap <silent> <home> <C-O>:call Home()<CR>
nnoremap <silent> <home> :call Home()<CR>
function Home()
    let curcol = wincol()
    normal ^
    let newcol = wincol()
    if newcol == curcol
        normal 0
    endif
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""
" file type handling
""""""""""""""""""""""""""""""""""""""""""""""""

" To create new file securely do: vim new.file.txt.gpg
" Your private key used to decrypt the text before viewing should
" be protected by a passphrase. Alternatively one could use
" a passphrase directly with symmetric encryption in the gpg commands below.
au BufNewFile,BufReadPre *.gpg :set secure viminfo= noswapfile nobackup nowritebackup history=0 binary
au BufReadPost *.gpg :%!gpg -d 2>/dev/null
au BufWritePre *.gpg :%!gpg -e -r 'P@draigBrady.com' 2>/dev/null
au BufWritePost *.gpg u

filetype on
filetype plugin on
filetype indent on
filetype plugin indent on

""""""""""""""""""""""""""""""""""""""""""""""""
" other settings
""""""""""""""""""""""""""""""""""""""""""""""""
"cursor highlight
set cursorline
"display cloumn line
set cursorcolumn
set ttyfast
set ruler
"set backspace=indent,eol,start
" relative line number
"set relativenumber
"unlimited undo
"set undofile
"wrap lines
set wrap

set incsearch
set showmatch
"highlight search results
set hlsearch

"correctly display too long lines
set textwidth=80
set formatoptions=qrnl
"set colorcolumn=85

"move key maps in insert mode
inoremap <c-j> <down>
inoremap <c-k> <up>
inoremap <c-l> <right>
inoremap <c-h> <left>
inoremap <c-d> <bs>

"regular prefix change
nnoremap / /\v
vnoremap / /\v

" easily move in buffers
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-l> :wincmd l<CR>

"cd to current diretory
nmap <leader>cd :execute "cd" expand("%:h")<CR>"

""""""""""""""""""""""""""""""""""""""""""""""""
" augroup settings
""""""""""""""""""""""""""""""""""""""""""""""""
augroup html
    au!
    "Disable parenthesis matching as it's way too slow
    au BufRead *.html NoMatchParen
    "Disable autoindenting for HTML as it's a bit annoying
    au FileType html set nosmartindent noautoindent
augroup END

augroup sh
    au!
    "smart indent really only for C like languages
    au FileType sh set nosmartindent autoindent
augroup END

augroup Python
    "See $VIMRUNTIME/ftplugin/python.vim
    au!
    "smart indent really only for C like languages
    "See $VIMRUNTIME/indent/python.vim
    au FileType python set nosmartindent autoindent
    " Allow gf command to open files in $PYTHONPATH
    au FileType python let &path = &path . "," . substitute($PYTHONPATH, ';', ',', 'g')
    if v:version >= 700
        "See $VIMRUNTIME/autoload/pythoncomplete.vim
        "<C-x><C-o> to autocomplete
        au FileType python set omnifunc=pythoncomplete#Complete
        "Don't show docs in preview window
        au FileType python set completeopt-=preview
    endif
augroup END

augroup man
    au!
    "Ensure vim is not recursively invoked (man-db does this)
    "when doing ctrl-[ on a man page reference
    au FileType man let $MANPAGER=""
augroup END

